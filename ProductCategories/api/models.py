# import signals

from django.db import models
from mptt.models import MPTTModel, TreeForeignKey




class Category(MPTTModel):
    category_name = models.CharField(max_length=50, unique=True)
    parent = TreeForeignKey('self', null=True, blank=True,
                            related_name='children', db_index=True)

    class MPTTMeta:
        order_insertion_by = ['category_name']

    def __unicode__(self):
        return self.category_name


class Product(models.Model):
    price = models.DecimalField(max_digits=10, decimal_places=2, null=True)

    title = models.CharField(max_length=255)

    description = models.CharField(max_length=255)

    category = models.ForeignKey(Category)

    image = models.ImageField(blank=True, null=True)

    url = models.URLField(null=True)

    def __unicode__(self):
        return "{0} - {1}".format(self.title, self.price)
