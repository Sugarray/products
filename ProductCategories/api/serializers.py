from django.contrib.auth.models import User

from rest_framework import serializers

from api.models import Product, Category


class CategorySerializer(serializers.ModelSerializer):
    class Meta:
        model = Category
        fields = ('id', 'category_name', 'parent')


class DetailedProductSerializer(serializers.ModelSerializer):
    category_id = serializers.PrimaryKeyRelatedField(read_only=True)
    category = serializers.StringRelatedField(read_only=True)

    class Meta:
        model = Product
        fields = ('id', 'category_id', 'category', 'title',
                  'price', 'description', 'image')


class CreateProductSerializer(serializers.ModelSerializer):
    class Meta:
        model = Product
        fields = ('id', 'category', 'title',
                  'price', 'description', 'image')


class ProductSerializer(serializers.ModelSerializer):
    url = serializers.URLField(read_only=True)
    image = serializers.ImageField(use_url=True)

    class Meta:
        model = Product
        fields = (
            'id', 'category', 'url', 'title', 'price', 'image')


class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ('username', 'password',)
