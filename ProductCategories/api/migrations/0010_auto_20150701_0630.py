# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('api', '0009_product_product_details'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='product',
            name='product_details',
        ),
        migrations.AddField(
            model_name='product',
            name='url',
            field=models.URLField(null=True),
        ),
    ]
