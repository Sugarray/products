# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('api', '0008_remove_product_url'),
    ]

    operations = [
        migrations.AddField(
            model_name='product',
            name='product_details',
            field=models.ForeignKey(to='api.Product', null=True),
        ),
    ]
