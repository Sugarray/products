from django.contrib.auth import authenticate, login, logout
from rest_framework import viewsets
from rest_framework.response import Response
from rest_framework import status
from rest_framework.permissions import IsAuthenticated
from rest_framework.authentication import SessionAuthentication, \
    BasicAuthentication
from rest_framework.reverse import reverse_lazy

from models import Product, Category
from django.views.generic import TemplateView
import serializers
import cache


class ProductViewSet(viewsets.ModelViewSet):
    """Product API"""

    serializer_class = serializers.CreateProductSerializer
    queryset = Product.objects.all()
    permission_classes = (IsAuthenticated,)

    authentication_classes = (SessionAuthentication, BasicAuthentication)

    def list(self, request, *args, **kwargs):
        queryset = Product.objects.all()
        serializer = serializers.ProductSerializer(queryset, many=True)

        return Response(serializer.data)

    def create(self, request, *args, **kwargs):
        """ Creates new product """
        if request.data['category'].isdigit():
            cat = Category.objects.get(id=request.data['category'])
        else:
            cat = Category.objects.get(category_name=request.data['category'])

        product = Product(category=cat,
                          price=request.data['price'],
                          description=request.data['description'],
                          title=request.data['title']
                          )

        product.image.save(str(request.FILES['image']),
                           content=request.FILES['image'])

        product.save()
        product.url = reverse_lazy('product_by_id', request=request,
                                   kwargs={'pk': product.id})
        product.save()
        return Response(status=status.HTTP_201_CREATED)

    def detail(self, request, *args, **kwargs):
        """ Returns detail about product """

        pk = kwargs['pk']
        queryset = cache.ProductDetailJob().get(pk=pk)
        serializer = serializers.DetailedProductSerializer(queryset)

        return Response(serializer.data)

    def filter(self, request, *args, **kwargs):
        """ Returns product list by category"""

        name = kwargs['cat_name']
        queryset = Product.objects.filter(category__category_name=name)

        serializer = serializers.ProductSerializer(queryset, many=True)

        return Response(serializer.data)


class CategoryViewSet(viewsets.ModelViewSet):
    """ Category API """

    serializer_class = serializers.CategorySerializer
    queryset = Category.objects.all()
    permission_classes = (IsAuthenticated,)

    authentication_classes = (SessionAuthentication, BasicAuthentication)

    def list(self, request, *args, **kwargs):
        queryset = cache.CategoryListJob().get()
        serializer = serializers.CategorySerializer(queryset, many=True)

        return Response(serializer.data)

    def filter(self, request, *args, **kwargs):
        """ Returns  list by category"""

        name = kwargs['cat_name']

        category = Category.objects.get(category_name=name)
        children = category.get_family()

        serializer = serializers.CategorySerializer(children, many=True)

        return Response(serializer.data)


class AuthViewSet(viewsets.ModelViewSet):
    """ Auth API """

    serializer_class = serializers.UserSerializer

    def create(self, request, *args, **kwargs):
        """ Login """

        username = request.DATA['username']
        password = request.DATA['password']

        user = authenticate(username=username, password=password)
        if user is not None:

            if user.is_active:
                login(request, user)
                return Response(status=status.HTTP_200_OK)
        else:
            return Response(status=status.HTTP_400_BAD_REQUEST)

    def destroy(self, request, *args, **kwargs):
        """ Logout """

        logout(request)

        return Response({})
