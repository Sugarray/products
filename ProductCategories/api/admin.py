from django.contrib import admin
from models import Product, Category
from django_mptt_admin.admin import DjangoMpttAdmin

class CategoryAdmin(DjangoMpttAdmin):
    tree_auto_open = True


admin.site.register(Product)
admin.site.register(Category, CategoryAdmin)
