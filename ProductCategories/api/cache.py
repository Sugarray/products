import requests
from cacheback.base import Job
import models


class CategoryListJob(Job, ):

    def fetch(self, *args, **kwargs):
        return models.Category.objects.all()


class ProductDetailJob(Job):

    def fetch(self, *args, **kwargs):
        return models.Product.objects.get(pk=kwargs['pk'])

    def key(self, *args, **kwargs):

        kwargs['pk'] = unicode(kwargs['pk'])
        return super(ProductDetailJob, self).key(args, kwargs)
