from django.conf.urls import patterns, url, include

import views

all_products = views.ProductViewSet.as_view({
    'get': 'list',
    'post': 'create',
})

product_by_id = views.ProductViewSet.as_view({
    'get': 'detail',
    'delete': 'destroy',
    'put': 'update',
    'patch': 'partial_update',
})

all_categories = views.CategoryViewSet.as_view({
    'get': 'list',
    'post': 'create',
})

category_by_id = views.CategoryViewSet.as_view({
    'get': 'retrieve',
    'delete': 'destroy',
})

filtered_categories = views.CategoryViewSet.as_view({
    'get': 'filter',
})

filtered_products = views.ProductViewSet.as_view({
    'get': 'filter',
})

login = views.AuthViewSet.as_view({
    'post': 'create'
})

logout = views.AuthViewSet.as_view({
    'delete': 'destroy'
})

auth = views.AuthViewSet

product_urls = patterns('',
                        url(r'^$', all_products, name='all_products'),

                        url(r'^(?P<pk>\d+)/$', product_by_id,
                            name='product_by_id'),

                        url(r'^filter/(?P<cat_name>[0-9a-zA-Z_-]+)/$',
                            filtered_products, name='filtered_products'),
                        )

category_urls = patterns('',
                         url(r'^$', all_categories, name='all_categories'),

                         url(r'^(?P<pk>\d+)/$', category_by_id),

                         url(r'^filter/(?P<cat_name>[0-9a-zA-Z_-]+)/$',
                             filtered_categories, name='filtered_categories'),
                         )

auth_urls = patterns('',
                     url(r'^login/$', login),
                     url(r'^logout/$', logout)
                     )

urlpatterns = patterns('',
                       url(r'^products/', include(product_urls)),
                       url(r'^categories/', include(category_urls)),
                       url(r'^auth/', include(auth_urls))
                       )
