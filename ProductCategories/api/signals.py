import models
from cache import CategoryListJob, ProductDetailJob
from django.db.models.signals import post_delete, post_save
from django.dispatch import receiver

invalidate_signals = [post_delete, post_save]

@receiver(invalidate_signals, sender=models.Category)
def invalidate_category(sender, instance, **kwargs):

    CategoryListJob().invalidate()


@receiver(invalidate_signals, sender=models.Product)
def invalidate_product(sender, instance, **kwargs):

    ProductDetailJob().invalidate(pk=instance.pk)
