from django.conf.urls import include, url, patterns
from django.contrib import admin
from ProductCategories import settings
import views
from django.views.static import serve
from django.conf.urls.static import static

urlpatterns = [
    url(r'^admin/', include(admin.site.urls)),
    url(r'^api/', include('api.urls')),
    url(r'^docs/', include('rest_framework_swagger.urls')),
    url(r'^$', views.MainPageView.as_view()),

    url(r'^%s(?P<path>.*)$' % settings.MEDIA_URL[1:], serve,
        {'document_root': settings.MEDIA_ROOT}),

] + static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)

if settings.DEBUG:
    import debug_toolbar

    urlpatterns += patterns('',
                            url(r'^__debug__/', include(debug_toolbar.urls)),
                            )
